<?php
/**
 * OpenOTP Authentication Drupal module
 *
 * LICENSE
 *
 * Copyright © 2013.
 * RCDevs OpenOTP. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * *
 * @copyright Copyright (c) 2013 RCDevs (http://www.rcdevs.com)
 * @author rcdevs <info@rcdevs.com>
 * @package OpenOTP-7.x
 */
 
/*
 * @file
 * Admin and user page callbacks for OpenOTP module.
 */
function openotp_admin_settings($form_id, &$form_state) {
  
  drupal_add_js(drupal_get_path('module', 'openotp') . '/openotp.js', 'file');  
  
  $form = array(
    '#submit'        => array('openotp_admin_settings_submit'),
  );
    
  $form['openotp module desc'] = array(
    '#type'          => 'item',
    '#value'         => t('Enter your OpenOTP server settings in the fields below. You can also enable and force Two-Factor Authentication by editing the user on the Users page, and then clicking "Enable OpenOTP" button on their settings'),
  );
  $form['openotp_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable OpenOTP authentication'),
    '#default_value' => variable_get('openotp_enabled', FALSE),
  );

  //** Global configuration **//
  $form['openotp_global'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('OpenOTP Global settings'),
    '#description'   => t('Enter your OpenOTP server settings in the fields below. You can also enable and force Two-Factor Authentication by editing the user on the Users page, and then clicking "Enable OpenOTP" button on their settings.'),
    '#tree'          => TRUE,
    '#prefix'        => '<div id="openotp_settings_wrapper">'	
  );
  // Server URL.
  $form['openotp_global']['server_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Server URL'),
    '#default_value' => variable_get('openotp_global_server_url', 'http://myserver:8080/openotp'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => TRUE,
  );
  // Cient ID.
  $form['openotp_global']['client_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Client ID'),
    '#default_value' => variable_get('openotp_global_client_id', 'Drupal'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => TRUE,
  );
  // Default Domain.
  $form['openotp_global']['default_domain'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Default Domain'),
    '#default_value' => variable_get('openotp_global_default_domain'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => FALSE,
  );
  // User Settings.
  $form['openotp_global']['user_settings'] = array(
    '#type'          => 'textfield',
    '#title'         => t('User settings'),
    '#default_value' => variable_get('openotp_global_user_settings'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => FALSE,
  );
  // Disable Super admin.
  $form['openotp_global']['disable_super_admin'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Disable two-factor for user Admin (uid = 1)'),
    '#default_value' => variable_get('openotp_global_disable_super_admin',1),
    '#suffix'        => '<small>By default in Drupal User #1 has all privileges, so it\'s not possible to disable two-factor for him in permission section</small>'
	
  );
  

   //** Proxy configuration **//
  $form['openotp_proxy'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('OpenOTP Proxy settings'),
    '#description'   => t('Enter your OpenOTP proxy settings in the fields below.'),
    '#tree'          => TRUE,
    '#required'      => FALSE,	
  );
   // Proxy Host.
  $form['openotp_proxy']['host'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Proxy Host'),
    '#default_value' => variable_get('openotp_global_host'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => FALSE,
  );
   // Proxy Port.
  $form['openotp_proxy']['port'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Proxy Port'),
    '#default_value' => variable_get('openotp_global_port'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => FALSE,
  );
   // Proxy Host.
  $form['openotp_proxy']['login'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Proxy Login'),
    '#default_value' => variable_get('openotp_global_login'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => FALSE,
  );
   // Proxy Host.
  $form['openotp_proxy']['password'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Proxy Password'),
    '#default_value' => variable_get('openotp_global_password'),
    '#size'          => 40,
    '#maxlength'     => 128,
    '#required'      => FALSE,
    '#suffix'        => '</div>'
  );
  
    
  return system_settings_form($form);
 
}


function openotp_admin_settings_submit($form_id, &$form_state) {
  if ($form_state['values']['op'] == t('Reset to defaults')) {
    variable_del('openotp_enabled');
    variable_del('openotp_global_server_url');
    variable_del('openotp_global_client_id');
    variable_del('openotp_global_default_domain');
    variable_del('openotp_global_user_settings');
    variable_del('openotp_global_disable_super_admin');
    variable_del('openotp_proxy_host');
    variable_del('openotp_proxy_port');
    variable_del('openotp_proxy_login');
    variable_del('openotp_proxy_password');
  } 
  else {
	  
    module_load_include('inc', 'openotp.class');
	$openotp_enabled = $form_state['values']['openotp_enabled'];
	$openotp_global_server_url = $form_state['values']['openotp_global']['server_url'];
	$openotp_global_client_id = $form_state['values']['openotp_global']['client_id'];
	$openotp_global_default_domain = $form_state['values']['openotp_global']['default_domain'];
	$openotp_global_user_settings = $form_state['values']['openotp_global']['user_settings'];
	$openotp_global_disable_super_admin = $form_state['values']['openotp_global']['disable_super_admin'];
	$openotp_proxy_host = $form_state['values']['openotp_proxy']['host'];
	$openotp_proxy_port = $form_state['values']['openotp_proxy']['port'];
	$openotp_proxy_login = $form_state['values']['openotp_proxy']['login'];
	$openotp_proxy_password = $form_state['values']['openotp_proxy']['password'];
	
    variable_set('openotp_enabled', $openotp_enabled);
    variable_set('openotp_global_server_url', $openotp_global_server_url);
    variable_set('openotp_global_client_id', $openotp_global_client_id);
    variable_set('openotp_global_default_domain', $openotp_global_default_domain);
    variable_set('openotp_global_user_settings', $openotp_global_user_settings);
    variable_set('openotp_global_disable_super_admin', $openotp_global_disable_super_admin);
    variable_set('openotp_proxy_host', $openotp_proxy_host);
    variable_set('openotp_proxy_port', $openotp_proxy_port);
    variable_set('openotp_proxy_login', $openotp_proxy_login);
    variable_set('openotp_proxy_password', $openotp_proxy_password);
  }
}


// OpenOTP Management

/**
 * Menu callback; Enable OpenOTP per user.
 */
function openotp_user_setting($account) {
  drupal_set_title($account->name);
  	/*TODO*/
	/*Show otp setting status according to role/global inheritance*/
	$build['openotp_table'] = array(
		'#type' => 'markup',
		//'#markup' => '<p>OpenOTP is enabled for this user</p>',
		'#markup' => '',
	);  
  
  $build['openotp_user_setting'] = drupal_get_form('openotp_user_add');
  return $build;
  return true;
}

/**
 * Form builder; Add OpenOTP field to user edit page.
 */
function openotp_user_add() {
	
  $openotp_user_enabled_options = array(
    '0'		=> t('Inherit Configuration'),
    '1'		=> t('Enable one Time Password'),
    '2'		=> t('Disable one Time Password'),
  );
  $form['openotp_user_enabled'] = array(
    '#type'          => 'radios',
    '#title'         => t('Enable/Disable OpenOTP authentication'),
    '#default_value' => variable_get('openotp_user_enabled', '0'),
    '#options'       => $openotp_user_enabled_options,
  );
	
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save setting'));

  return $form;
}

/**
 * Add a OpenOTP identity.
 */
function openotp_user_add_submit($form, &$form_state) {

	if (!form_get_errors()) {

		$user_enabled = $form_state['values']['openotp_user_enabled']; 	
		variable_set('openotp_user_enabled', $user_enabled);
		
		 $id = db_merge('openotp_user_enabled')
		  ->key(array('uid' => arg(1)))
		  ->insertFields(array(
			'uid' => arg(1),	  
			'enabled' => $user_enabled,
		  ))
		  ->updateFields(array(
			'enabled' => $user_enabled,
		  ))
		  ->execute();		
		
		if ($id) {
		$user = openotp_get_user(arg(1));
		$variables = array('%name' => $user->name);
		drupal_set_message(t('OpenOTP status has been updated.', $variables)); 
		$type = ($user_enabled == 1 ? "Enable" : ($user_enabled == 2 ? "Disable" : "Inherit"));
		watchdog('openotp', $type.' OpenOTP for user %name.', $variables, WATCHDOG_NOTICE);
	}
  }
}

?>