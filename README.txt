**************************************************************************
                  O P E N O T P   M O D U L E
**************************************************************************
Name: OpenOTP Authentication Module
Author: rcdevs <info@rcdevs.com>
Drupal: 7.x 
**************************************************************************

NOTE: This Plugin enables strong two factor authentication for admins 
and users. 
     
DESCRIPTION:

It displays an overlay on Challenge-Response session, after fill in 
username and password. The plugin supports global, per role and per 
user settings configuration. 

The plugin will transparently handle any OpenOTP Login Mode including
LDAP only, OTP only and LDAP+OTP. 

Permissions (per role) to:
- Perform administration tasks for OpenOTP
- Allow users to enable two-factor on their profile settings page
- Enable two-factor authentication for user role

By default in Drupal User #1 has all privileges, a setting allow to 
enable or not two-factor for super admin user

OPENOTP SERVER:

OpenOTP is the RCDevs user authentication solution. OpenOTP is a server
application which provides multiple (highly configurable) authentication
schemes for your LDAP users, based on one-time passwords (OTP) technologies
 and including: - OATH HOTP/TOTP/OCRA Software/Hardware Tokens - Google 
Authenticator - Mobile-OTP (mOTP) Software Tokens - SMS One-Time Passwords
- Mail / Secure Mail One-Time Passwords - Yubikey

OpenOTP provides both SOAP/XML and RADIUS client APIs.


**************************************************************************

INSTALLATION:

1. Download the module and extract it in the folder sites/all/modules/contrib. 
It is recommended to place all third-party modules in a subfolder called contrib.

2. Go to the Module page at Administer > Modules (http://example.com/admin/modules) 
and enable it.

3. Read the module's documentations (readme file or on-line here on drupal.org) for 
further configuration instructions.

Note: The drush commands drush dl module_name and drush en module_name offer probably 
the fastest way installing modules.

**************************************************************************










