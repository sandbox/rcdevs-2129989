(function ($) {
  $(document).ready(function() {
    if(!$('#edit-openotp-enabled').is(':checked')) {
      $('#openotp_settings_wrapper').hide();
    }
    $('#edit-openotp-enabled').click(function() {
      if($(this).is(':checked')) {
         $('#openotp_settings_wrapper').slideDown('fast');
      } else {
         $('#openotp_settings_wrapper').slideUp('fast');
      }
    });   
  });
})(jQuery);
